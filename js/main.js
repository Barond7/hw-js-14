jQuery(document).ready(function () {
    jQuery('a.scrollto').click(function () {
        elementClick = jQuery(this).attr('href');
        destination = jQuery(elementClick).offset().top;
        jQuery('html:not(:animated), body:not(:animated)').animate({
            scrollTop: destination
        }, 1500);
        return false;
    });
});
$(document).ready(function () {
    const height = document.documentElement.clientHeight;
    const button = $('#button-up');
    $(window).scroll(function () {
        if ($(this).scrollTop() > height) {
            button.fadeIn();
        } else {
            button.fadeOut();
        }
    });
    button.on('click', function () {
        $('body, html').animate({
            scrollTop: 0
        }, 3500);
        return false;
    });
});

$('#toggle').click(function () {
    $('.animated').slideToggle(1000);

});